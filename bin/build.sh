#!/bin/sh

BUBBLEWRAP_VERSION=1.21.0
REPO_NAME=lucidmodules/bubblewrap-$BUBBLEWRAP_VERSION
IMAGE_TAG=$BUBBLEWRAP_VERSION

docker build -t $REPO_NAME:$IMAGE_TAG -f docker/bubblewrap-$BUBBLEWRAP_VERSION/Dockerfile docker/bubblewrap-$BUBBLEWRAP_VERSION/
