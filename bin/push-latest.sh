#!/bin/sh

BUBBLEWRAP_VERSION=1.21.0
REPO_NAME=lucidmodules/bubblewrap-$BUBBLEWRAP_VERSION
IMAGE_TAG=$BUBBLEWRAP_VERSION

docker pull $REPO_NAME:$IMAGE_TAG
docker tag $REPO_NAME:$IMAGE_TAG $REPO_NAME:latest
docker image push $REPO_NAME:latest
