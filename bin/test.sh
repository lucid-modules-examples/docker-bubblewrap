#!/bin/sh

BUBBLEWRAP_VERSION=1.21.0
REPO_NAME=lucidmodules/bubblewrap-$BUBBLEWRAP_VERSION
IMAGE_TAG=$BUBBLEWRAP_VERSION

docker run --rm $REPO_NAME:$IMAGE_TAG bubblewrap help
