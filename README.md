# Docker Image Lucid Modules Bubblewrap
The Docker repository must be created in the Docker Registry prior to push.
Containerizes the [Google Bubblewrap](https://github.com/GoogleChromeLabs/bubblewrap) for building TWA from PWA.

## Usage
In your CI script you can execute `bubblewrap`, e.g.:
```shell
bubblewrap init --manifest https://my-twa.com/manifest.json
```

## CI Variables
- CI_REGISTRY_USER
- CI_REGISTRY_PASSWORD - this is the Docker Registry Access Token, not the account password
